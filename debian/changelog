libstring-dirify-perl (1.03-4) UNRELEASED; urgency=medium

  * Remove libmodule-build-perl from Build-Depends.
    This distribution uses ExtUtils::MakeMaker.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Jan 2023 18:42:16 +0100

libstring-dirify-perl (1.03-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libstring-dirify-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 21:12:35 +0100

libstring-dirify-perl (1.03-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl and
      perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 10:50:01 +0100

libstring-dirify-perl (1.03-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 02:16:02 +0100

libstring-dirify-perl (1.03-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 1.03
  * bump debhelper compatibility level to 9
  * Declare complaince with debian policy 3.9.8
  * debian/control: build depends on versioned libtest-simple-perl

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 21 Jul 2016 12:10:13 -0300

libstring-dirify-perl (1.02-2) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * Add myself as uploader to avoid uploading without human maintainers

 -- Damyan Ivanov <dmn@debian.org>  Mon, 08 Jun 2015 13:51:34 +0000

libstring-dirify-perl (1.02-1) unstable; urgency=low

  * Initial Release. (Closes: #609043)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 08 Jan 2011 11:13:42 +0000
